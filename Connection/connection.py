import socket
import sqlite3

from CacheManager import cacheManager

class EstaiblishConnection(cacheManager):
    def connect(self):
        print("Server Side Communication : ")
        print("Waiting for connection....")
        self.host = socket.gethostname()
        self.port = 6379 

        self.server_socket = socket.socket()  
        self.server_socket.bind((self.host, self.port))
        
    def configureListener(self):
        # configure how many client the server can listen simultaneously

        while True: 
            self.server_socket.listen(20)
            data, addr = self.server_socket.accept()  
            print("Connection from: " + str(addr))  
            self.msg = data.recv(1024).decode()
            if not self.msg:
                break
            self.info = "Request Recieved"
            data.send(self.info.encode())
            self.key = self.getKeyFromMessage(self.msg)
            self.value = self.getValueFromMessage(self.msg)
            return self.msg
            
        
    def initializeDataBase(self):
        self.myconn = sqlite3.connect("test.db")
        return self.myconn
    
        
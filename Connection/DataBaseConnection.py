import sqlite3

  
class DataBaseConnection():
    
    def __init__(self):
        self.myconn = sqlite3.connect("test.db")

    def executeQuery(self, query):  
        self.myconn.execute(query)
        self.myconn.commit()
        
    def get(self, key):
        self.executeQuery("SELECT VALUE FROM Data WHERE ID =  %s;" %key)
        
    def put(self, key, value):
        self.executeQuery(f"INSERT INTO Data VALUES('{key}', '{value}');" )   
            
    def getAll(self):
        print("DB Data :\n")
        data = self.myconn.execute("SELECT * FROM Data;")
        for row in data:
            print("key:",row[0],end=" ")
            print("val:",row[1])
            
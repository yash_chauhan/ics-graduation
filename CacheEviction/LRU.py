from collections import OrderedDict
from DataBaseConnection import DataBaseConnection

class LRUCache(object):
 
	def initialization(self, capacity):
		self.cache = OrderedDict()
		self.capacity = capacity
  
	def getFromCache(self, key: int):
		if key not in self.cache:
			self.dbConnection = DataBaseConnection()
			print(self.dbConnection.get(key))
	
		else:
			self.cache.move_to_end(key)
			print(self.cache[key])

	def putIntoCache(self, key: int, value: int):
		self.cache[key] = value
		self.cache.move_to_end(key)
		if len(self.cache) > self.capacity:
			self.cache.popitem(last = False)

	def getCache(self):
		print(self.cache)


from Connection.connection import EstaiblishConnection
from CacheManager.CacheManager import cacheManager
from Connection.DataBaseConnection import DataBaseConnection

class Server(cacheManager):
    def __init__(self):
        self.initialization(2)
        estaiblishConnection = EstaiblishConnection()
        estaiblishConnection.connect()
        self.myconn = estaiblishConnection.initializeDataBase()
        self.msg = estaiblishConnection.configureListener()  
            
server = Server()
key = server.getKeyFromMessage(server.msg)
value = server.getValueFromMessage(server.msg)
server.putValue(key, value)
server.getValue(key)
server.getCache()
dbconnect = DataBaseConnection()
dbconnect.getAll()
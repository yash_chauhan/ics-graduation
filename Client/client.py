import socket

class Client:
    dict = {}
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.client_socket= socket.socket()  
        self.client_socket.connect((self.host, self.port))
           
    def consoleInput(self): 
        while (True):
            command = input("Enter command:\t")
            if command == 'ics_get':
                key = input("Enter Key:\t")
                self.getValue(key)
                
            elif command == 'ics_put':
                key = input("Enter Key:\t")    
                value = input("Enter Value:\t")
                self.putValue(key+":"+value)
                
            else:
                print("Enter correct command")
                self.client_socket.close()
                break
       
    
    def addValue(self):
        print("Do you want to add more Y/N?")
        if(input()=="Y"):
           self.putValue() 
      
    def sendMessagetoServer(self,msg):
        self.client_socket.send(msg)  
            
    def putValue(self,msg):
        self.sendMessagetoServer(msg)
        msg_rcv=self.client_socket.recv(1024)
        print(msg_rcv.decode())
                      
    def getValue(self,key):
        self.sendMessagetoServer(key)
        msg_rcv=self.client_socket.recv(1024)
        print(msg_rcv.decode())
        
obj= Client (socket.gethostname(), 6379)
obj.consoleInput()



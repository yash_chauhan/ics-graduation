from LRU import LRUCache


class cacheManager(LRUCache):
        
    def getValue(self, key):
        self.getFromCache(key)

    def putValue(self, key, value):
        self.putIntoCache(key, value)
        
    def getKeyFromMessage(self, msg):
        return(msg.split(":")[0])

    def getValueFromMessage(self, msg):
        return(msg.split(":")[1])
    
    def getAllDbData(self):
        self.dbConnection.getAll()
        
    
    